DROP VIEW ProjectDetails;
DROP VIEW RoleDetails;

CREATE VIEW ProjectDetails AS 
SELECT i.item_name, 
i.description, 
p.description project_description, p.project_name item_description
FROM PROJECTITEMS i
JOIN Projects p
ON p.project_code = i.project_code;

CREATE VIEW RoleDetails AS
SELECT pmr.username, r.role_name, pi.item_name, t.tech_name
FROM 
ProjectMemberRoles pmr
JOIN ProjectRoles r ON
r.role_code = pmr.ROLE_CODE
JOIN projectItems pi
ON pmr.ITEM_CODE = pi.ITEM_CODE
JOIN PROJECTTECHNOLOGIES pt
ON pt.PROJECT_ITEM_CODE = pi.ITEM_CODE
JOIN technologies t ON
t.code = pt.TECH_CODE
;