DROP TABLE PortfolioWork.wk_ProjectMemberRoles;
DROP TABLE PortfolioWork.wk_ProjectRoles;
DROP TABLE PortfolioWork.wk_Screenshots;
DROP TABlE PortfolioWork.wk_ProjectTechnologies;
DROP TABLE PortfolioWork.wk_ProjectItems;
DROP TABLE PortfolioWork.wk_Statuses;
DROP TABLE PortfolioWork.wk_Technologies;
DROP TABLE PortfolioWork.wk_Projects;
DROP TABLE PortfolioWork.wk_Members;
DROP SEQUENCE PortfolioWork.GEN_SEQ;

CREATE TABLE PortfolioWork.wk_Members (
username VARCHAR2(20) PRIMARY KEY,
firstname VARCHAR2(20),
lastname VARCHAR2(20),
p_word VARCHAR2(15),
bio VARCHAR2(250) DEFAULT 'No bio selected',
profile_photo BLOB DEFAULT null
);


CREATE TABLE PortfolioWork.wk_Projects(
project_name VARCHAR2(20),
project_code NUMBER(2) PRIMARY KEY,
description VARCHAR(200)
);


CREATE TABLE PortfolioWork.wk_ProjectRoles(
role_name VARCHAR2(20),
role_code NUMBER(2) PRIMARY KEY
);

CREATE TABLE PortfolioWork.wk_Technologies(
tech_name VARCHAR2(20),
code NUMBER(2) PRIMARY KEY
);

CREATE TABLE PortfolioWork.wk_Statuses(
code VARCHAR2(30) PRIMARY KEY,
status_name VARCHAR2(30));

CREATE TABLE PortfolioWork.wk_ProjectItems(
item_name VARCHAR2(20),
item_code NUMBER(2) PRIMARY KEY,
project_code NUMBER(2) CONSTRAINT fk_pi_project_code REFERENCES PortfolioWork.wk_Projects (project_code),
description VARCHAR2(200),
status VARCHAR2(30) CONSTRAINT fk_pi_status REFERENCES PortfolioWork.wk_Statuses(code)
);

CREATE TABLE PortfolioWork.wk_ProjectTechnologies(
project_item_code NUMBER(2)CONSTRAINT fk_t_project_item_code REFERENCES PortfolioWork.wk_ProjectItems (item_code),
tech_code NUMBER(2) CONSTRAINT fk_t_tech_code REFERENCES PortfolioWork.wk_Technologies (code),
member_name VARCHAR(20) CONSTRAINT fk_t_member_name REFERENCES PortfolioWork.wk_Members (username),
PRIMARY KEY (project_item_code, tech_code, member_name)
);

CREATE TABLE PortfolioWork.wk_Screenshots(
s_code NUMBER(2) PRIMARY KEY,
s_location BLOB,
description  VARCHAR2(200),
item_code NUMBER(2) CONSTRAINT fk_s_item_code REFERENCES PortfolioWork.wk_ProjectItems(item_code)
);


CREATE TABLE PortfolioWork.wk_ProjectMemberRoles(
username VARCHAR2(20) CONSTRAINT fk_pmr_username REFERENCES PortfolioWork.wk_Members (username),
item_code NUMBER(2) CONSTRAINT fk_pmr_item_code REFERENCES PortfolioWork.wk_ProjectItems (item_code),
role_code NUMBER(2) CONSTRAINT fk_pmr_role_code REFERENCES PortfolioWork.wk_ProjectRoles (role_code),
PRIMARY KEY (username,item_code,role_code)
);

CREATE SEQUENCE PortfolioWork.GEN_SEQ START WITH 1 INCREMENT BY 1;
