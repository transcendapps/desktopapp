DROP TABLE ProjectMemberRoles;
DROP TABLE ProjectRoles;
DROP TABLE Screenshots;
DROP TABLE ProjectTechnologies;
DROP TABLE ProjectItems;
DROP TABLE Statuses;
DROP TABLE Technologies;
DROP TABLE Projects;
DROP TABLE Members;
DROP SEQUENCE GEN_SEQ;

CREATE TABLE Members (
username VARCHAR2(20) PRIMARY KEY,
firstname VARCHAR2(20),
lastname VARCHAR2(20),
p_word VARCHAR2(15),
bio VARCHAR2(250) DEFAULT 'No bio selected',
profile_photo BLOB DEFAULT null
);


CREATE TABLE Projects(
project_name VARCHAR2(20),
project_code NUMBER(2) PRIMARY KEY,
description VARCHAR2(200)
);


CREATE TABLE ProjectRoles(
role_name VARCHAR2(20),
role_code NUMBER(2) PRIMARY KEY
);

CREATE TABLE Technologies(
tech_name VARCHAR2(20),
code NUMBER(2) PRIMARY KEY
);

CREATE TABLE Statuses(
code VARCHAR2(30) PRIMARY KEY,--prefix this with s, t, or p so we know which table they belong to
status_name VARCHAR(30));

CREATE TABLE ProjectItems(
item_name VARCHAR2(20),
item_code NUMBER(2) PRIMARY KEY,
project_code NUMBER(2) CONSTRAINT fk_pi_project_code REFERENCES Projects (project_code),
description VARCHAR2(200),
status VARCHAR2(30) CONSTRAINT fk_pi_status REFERENCES Statuses(code)
);

CREATE TABLE ProjectTechnologies(
project_item_code NUMBER(2)CONSTRAINT fk_t_project_item_code REFERENCES ProjectItems (item_code),
tech_code NUMBER(2)CONSTRAINT fk_t_tech_code REFERENCES Technologies (code),
member_name VARCHAR(20) CONSTRAINT fk_t_member_name REFERENCES Members (username),
PRIMARY KEY (project_item_code, tech_code, member_name)
);


CREATE TABLE Screenshots(
s_code NUMBER(2) PRIMARY KEY,
s_location BLOB,
description  VARCHAR2(200),
item_code NUMBER(2) CONSTRAINT fk_s_item_code REFERENCES ProjectItems(item_code)
);


CREATE TABLE ProjectMemberRoles(
username VARCHAR2(20) CONSTRAINT fk_pmr_username REFERENCES Members (username),
item_code NUMBER(2) CONSTRAINT fk_pmr_item_code REFERENCES ProjectItems (item_code),
role_code NUMBER(2) CONSTRAINT fk_pmr_role_code REFERENCES ProjectRoles (role_code),
PRIMARY KEY (username,item_code,role_code)
);

CREATE SEQUENCE GEN_SEQ START WITH 1 INCREMENT BY 1;
