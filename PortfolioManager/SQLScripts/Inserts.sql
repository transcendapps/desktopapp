insert into members values('Lornita','Lorna','Ryan','pass',default,default);

insert into projectroles values('Development',1);
insert into projectroles values('Design',2);

insert into statuses values ('p_backlog','Backlog');
insert into statuses values ('p_progress','In progress');
insert into statuses values ('p_complete','Complete');

insert into projects values ('Android app', 1, 'App development');
insert into technologies values ('C#',1);
insert into projectitems values('Budget Tool', 1, 1,'Budgeting app', 'p_progress');
insert into projectmemberroles values ('Lornita', 1,1);

insert into projecttechnologies values (1,1,'Lornita');

