CREATE OR REPLACE PACKAGE pkg_add_portfolio_manager
AS
  user_exists    EXCEPTION;
  project_exists EXCEPTION;
  PROCEDURE add_user(
      p_uname IN members.username%TYPE,
      p_fname IN members.firstname%TYPE,
      p_lname IN members.lastname%TYPE,
      p_pword IN members.p_word%TYPE,
      p_bio   IN members.bio%TYPE,
      p_photo IN members.profile_photo%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE add_project(
      p_project_name IN projects.project_name%TYPE,
      p_description  IN projects.description%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE add_project_item(
      p_item_name       IN projectitems.item_name%TYPE,
      p_project_code    IN projectitems.project_code%TYPE,
      p_description     IN projectitems.description%TYPE,
      p_status          IN projectitems.status%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE add_project_member_role(
      p_username  IN projectmemberroles.username%TYPE,
      p_item_code IN projectmemberroles.item_code%TYPE,
      p_role_code IN projectmemberroles.role_code%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE add_screenshot(
      p_s_location  IN screenshots.s_location%TYPE,
      p_description IN screenshots.description%TYPE,
      p_item_code   IN screenshots.item_code%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE add_technology(
      p_name IN technologies.tech_name%TYPE,
      error_message OUT VARCHAR2
  );
END;
/
CREATE OR REPLACE PACKAGE BODY pkg_add_portfolio_manager
AS
  PROCEDURE add_user(
      p_uname IN members.username%TYPE,
      p_fname IN members.firstname%TYPE,
      p_lname IN members.lastname%TYPE,
      p_pword IN members.p_word%TYPE,
      p_bio   IN members.bio%TYPE,
      p_photo IN members.profile_photo%TYPE,
      error_message OUT VARCHAR2)
  AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(*) INTO v_count FROM members WHERE username = p_uname;
    IF v_count > 0 THEN
      RAISE user_exists;
    END IF;
    INSERT
    INTO members VALUES
      (
        p_uname,
        p_fname,
        p_lname,
        p_pword,
        p_bio,
        p_photo
      );
    error_message:= 'User added successfully';
  EXCEPTION
  WHEN user_exists THEN
    DBMS_OUTPUT.PUT_LINE('User already exists');
    error_message:='User already exists';
  END add_user;
  PROCEDURE add_project
    (
      p_project_name IN projects.project_name%TYPE,
      p_description  IN projects.description%TYPE,
      error_message OUT VARCHAR2
    )
  AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM projects
    WHERE project_name = p_project_name;
    IF v_count         > 0 THEN
      RAISE project_exists;
    END IF;
    INSERT INTO projects VALUES
      (p_project_name, GEN_SEQ.NEXTVAL, p_description
      );
    error_message:= 'Project added successfully';
  EXCEPTION
  WHEN project_exists THEN
    DBMS_OUTPUT.PUT_LINE('Project already exists');
    error_message:='Project already exists';
  END add_project;
  PROCEDURE add_project_item
    (
      p_item_name       IN projectitems.item_name%TYPE,
      p_project_code    IN projectitems.project_code%TYPE,
      p_description     IN projectitems.description%TYPE,
      p_status          IN projectitems.status%TYPE,
      error_message OUT VARCHAR2
    )
  AS
  BEGIN
    INSERT
    INTO projectitems VALUES
      (
        p_item_name,
        GEN_SEQ.NEXTVAL,
        p_project_code,
        p_description,
        p_status
      );
      error_message:='Project item added successfully';
      EXCEPTION
  WHEN project_exists THEN
    DBMS_OUTPUT.PUT_LINE('Project item already exists');
    error_message:='Project item already exists';
  END add_project_item;
  PROCEDURE add_project_member_role
    (
      p_username  IN projectmemberroles.username%TYPE,
      p_item_code IN projectmemberroles.item_code%TYPE,
      p_role_code IN projectmemberroles.role_code%TYPE,
      error_message OUT VARCHAR2
    )
  AS
  BEGIN
    INSERT
    INTO projectmemberroles VALUES
      (
        p_username,
        p_item_code,
        p_role_code
      );
  END add_project_member_role;
  PROCEDURE add_screenshot
    (
      p_s_location  IN screenshots.s_location%TYPE,
      p_description IN screenshots.description%TYPE,
      p_item_code   IN screenshots.item_code%TYPE,
      error_message OUT VARCHAR2
    )
  AS
  BEGIN
    INSERT
    INTO screenshots VALUES
      (
        GEN_SEQ.NEXTVAL,
        p_s_location,
        p_description,
        p_item_code
      );
  END add_screenshot;
  
  PROCEDURE add_technology(
      p_name IN technologies.tech_name%TYPE,
      error_message OUT VARCHAR2
  )
  AS
  BEGIN
    INSERT INTO technologies VALUES(
      GEN_SEQ.NEXTVAL,
      p_name
    );
  END add_technology;
END pkg_add_portfolio_manager;
/
