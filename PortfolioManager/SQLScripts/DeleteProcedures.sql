CREATE OR REPLACE PACKAGE pkg_delete_portfolio_manager
AS
  item_exists    EXCEPTION;
  
  PROCEDURE delete_user(
      p_uname IN members.username%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE delete_project(
      p_project_code IN projects.project_code%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE delete_project_item(
      p_item_code    IN projectitems.item_code%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE delete_project_member_role(
      p_username  IN projectmemberroles.username%TYPE,
      p_item_code IN projectmemberroles.item_code%TYPE,
      p_role_code IN projectmemberroles.role_code%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE delete_screenshot(
      p_s_code  IN screenshots.s_code%TYPE,
      error_message OUT VARCHAR2);
  PROCEDURE delete_technology(
      p_code IN technologies.code%TYPE,
      error_message OUT VARCHAR2
  );
END;
/
CREATE OR REPLACE PACKAGE BODY pkg_delete_portfolio_manager
AS
  PROCEDURE delete_user(
      p_uname IN members.username%TYPE,
      error_message OUT VARCHAR2)
  AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(*) INTO v_count FROM members WHERE username = p_uname;
    IF v_count = 0 THEN
      RAISE item_exists;
    END IF;
    DELETE 
    FROM members 
    WHERE username = p_uname;
    error_message:= 'User deleted successfully';
  EXCEPTION
  WHEN item_exists THEN
    DBMS_OUTPUT.PUT_LINE('User does not exists');
    error_message:='User does not exists';
  END delete_user;
  PROCEDURE delete_project(
      p_project_code IN projects.project_code%TYPE,
      error_message OUT VARCHAR2
    )
  AS
    v_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM projects
    WHERE project_code = p_project_code;
    IF v_count         = 0 THEN
      RAISE item_exists;
    END IF;
    DELETE 
    FROM projects 
    WHERE
      project_code = p_project_code;
    error_message:= 'Project deleted successfully';
  EXCEPTION
  WHEN item_exists THEN
    DBMS_OUTPUT.PUT_LINE('Project does not exist');
    error_message:='Project does not exist';
  END delete_project;
  PROCEDURE delete_project_item
    (
      p_item_code IN projectitems.item_code%TYPE,
      error_message OUT VARCHAR2
    )
  AS
   v_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM projectitems
    WHERE item_code = p_item_code;
    IF v_count         = 0 THEN
      RAISE item_exists;
    END IF;
    DELETE
    FROM projectitems 
    WHERE item_code = p_item_code;
    error_message:= 'Project item deleted successfully';
  EXCEPTION
  WHEN item_exists THEN
    DBMS_OUTPUT.PUT_LINE('Project item does not exist');
    error_message:='Project item does not exist';
  END delete_project_item;
  
  PROCEDURE delete_project_member_role
    (
      p_username  IN projectmemberroles.username%TYPE,
      p_item_code IN projectmemberroles.item_code%TYPE,
      p_role_code IN projectmemberroles.role_code%TYPE,
      error_message OUT VARCHAR2
    )
  AS
  v_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM projectitems
    WHERE item_code = p_item_code;
    IF v_count         = 0 THEN
      RAISE item_exists;
    END IF;
    DELETE
    FROM  projectmemberroles WHERE
        username = p_username
        AND item_code = p_item_code
        AND role_code = p_role_code;
        error_message:='Item successfully deleted';
      EXCEPTION
  WHEN item_exists THEN
    DBMS_OUTPUT.PUT_LINE('Item does not exist');
    error_message:='Item does not exist';
  END delete_project_member_role;
  
  PROCEDURE delete_screenshot
    (
      p_s_code  IN screenshots.s_code%TYPE,
      error_message OUT VARCHAR2
    )
  AS
  v_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM screenshots
    WHERE s_code = p_s_code;
    IF v_count         = 0 THEN
      RAISE item_exists;
    END IF;
    DELETE
    FROM screenshots 
    WHERE s_code = p_s_code;
        error_message:='Item successfully deleted';
      EXCEPTION
  WHEN item_exists THEN
    DBMS_OUTPUT.PUT_LINE('Item does not exist');
    error_message:='Item does not exist';
  END delete_screenshot;
  
  PROCEDURE delete_technology(
      p_code IN technologies.code%TYPE,
      error_message OUT VARCHAR2
  )

  AS
  v_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_count
    FROM technologies
    WHERE code = p_code;
    IF v_count         = 0 THEN
      RAISE item_exists;
    END IF;
    DELETE
    FROM technologies 
    WHERE code = p_code;
        error_message:='Item successfully deleted';
      EXCEPTION
  WHEN item_exists THEN
    DBMS_OUTPUT.PUT_LINE('Item does not exist');
    error_message:='Item does not exist';
  END delete_technology;
END pkg_delete_portfolio_manager;
/
