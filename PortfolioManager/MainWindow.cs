﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioManager
{
    public partial class MainWindow : Form
    {
        SQLWriter writer = new SQLWriter();
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'portfolioDataSet.PROJECTS' table. You can move, or remove it, as needed.
            tbcMainWindow.TabPages.Remove(tbpShowRoles);
            tbcMainWindow.TabPages.Remove(tbpShowProjects);
            tbcMainWindow.TabPages.Remove(tbpUserProfile);
            
           /* tbcMainWindow.Appearance = TabAppearance.FlatButtons;
            tbcMainWindow.ItemSize = new Size(0, 1);
            tbcMainWindow.SizeMode = TabSizeMode.Fixed;*/



        }



        private void btnLogin_Click(object sender, EventArgs e)
        {
            string name = txtUsername.Text;
            string pword = txtPassword.Text;
            writer.openConnection();
            if(writer.checkLoginDetails(name, pword))
            {
                //pnlLogin.Hide();

                lblError.Text = "Success!";
                lblError.ForeColor = Color.Green;
                showProfile(name);

            }
            else
            {
                lblError.Text = "Invalid details";
                lblError.ForeColor = Color.Red;
            }
            writer.closeConnection();

        }

        private void showProfile(string name)
        {
            tbcMainWindow.TabPages.Add(tbpUserProfile);
            tbcMainWindow.TabPages.Remove(tbpLogin);
            lblProfileUsername.Text = name;
            lblProfileName.Text = writer.getFullName(name);
            lblBio.Text = writer.getBio(name);
            showProjects();
            tbcMainWindow.TabPages.Add(tbpShowRoles);
            tbcMainWindow.TabPages.Add(tbpShowProjects);
        }

        private void lnkNewUser_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            changeVisibility();
            
        }

        private void changeVisibility()
        {
            lblFirstName.Visible = true;
            lblLastName.Visible = true;
            lblConfirmPassword.Visible = true;
            txtFirstname.Visible = true;
            txtLastname.Visible = true;
            txtConfirmPassword.Visible = true;
            btnLogin.Visible = false;
            btnAddUser.Visible = true;
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            string uname = txtUsername.Text;
            string fname = txtFirstname.Text;
            string lname = txtLastname.Text;
            string pword = txtPassword.Text;
            string bio = " ";
            string photo = null;
            writer.openConnection();
            if (writer.userExists(uname))
            {
                lblError.Text = "Username already exists";
            }
            else
            {
                lblError.Text = writer.insertMember(uname, fname, lname, pword,bio,photo);
                showProfile(fname);

            }
            writer.closeConnection();
        }

        private void showProjects()
        {
            writer.populateProjectRoles(this.dgvRoleDetails,"RoleDetails");
            writer.populateProjectRoles(this.dgvProjects, "ProjectDetails");
        }

        private void lblAddProject_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtProjectName.Visible = true;
            cboProjectName.Visible = false;
            lblPDesc.Visible = true;
            txtPDesc.Visible = true;
            lblAddProject.Visible = false;
        }

        private void lblNewProject_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            lblProjectName.Visible = true;
            lblAddProject.Visible = true;
            lblPIDesc.Visible = true;
            lblPIName.Visible = true;
            lblStatus.Visible = true;
            lblTech.Visible = true;
            lblRole.Visible = true;

            cboProjectName.Visible = true;
            txtPIName.Visible = true;
            txtPIDesc.Visible = true;
            cboStatus.Visible = true;
            cboTech.Visible = true;
            cboRole.Visible = true;
            btnAddItem.Visible = true;
            dgvProjects.Visible = false;
            btnBack.Visible = true;
            lblAddTech.Visible = true;

            fillComboBoxes();

            //lblNewProject.Visible = false;

        }

        private void fillComboBoxes()
        {
            DataTable projectDT = new DataTable();
            DataTable roleDT = new DataTable();
            DataTable techDT = new DataTable();
            DataTable statusDT = new DataTable();

            writer.openConnection();
            projectDT = writer.fillProjectDataSource(projectDT);

            cboProjectName.DataSource = projectDT;
            cboProjectName.DisplayMember = projectDT.Columns[0].ToString();
            cboProjectName.ValueMember = projectDT.Columns[1].ToString();
            cboProjectName.BindingContext = new BindingContext();

            roleDT = writer.fillRoleDataSource(roleDT);
            cboRole.DataSource = roleDT;
            cboRole.DisplayMember = roleDT.Columns[0].ToString();
            cboRole.ValueMember = roleDT.Columns[1].ToString();
            cboRole.BindingContext = new BindingContext();

            techDT = writer.fillTechDataSource(techDT);
            cboTech.DataSource = techDT;
            cboTech.DisplayMember = techDT.Columns[0].ToString();
            cboTech.ValueMember = techDT.Columns[1].ToString();
            cboTech.BindingContext = new BindingContext();


            statusDT = writer.fillStatusDataSource(statusDT);
            cboStatus.DataSource = statusDT;
            cboStatus.DisplayMember = statusDT.Columns[0].ToString();
            cboStatus.ValueMember = statusDT.Columns[1].ToString();
            cboStatus.BindingContext = new BindingContext();

            writer.closeConnection();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            lblProjectName.Visible = false;
            lblAddProject.Visible = false;
            lblPIDesc.Visible = false;
            lblPIName.Visible = false;
            lblStatus.Visible = false;
            lblTech.Visible = false;
            lblRole.Visible = false;

            txtProjectName.Visible = false;
            lblPDesc.Visible = false;
            txtPDesc.Visible = false;

            cboProjectName.Visible = false;
            txtPIName.Visible = false;
            txtPIDesc.Visible = false;
            cboStatus.Visible = false;
            cboTech.Visible = false;
            cboRole.Visible = false;
            btnAddItem.Visible = false;
            dgvProjects.Visible = true;
            btnBack.Visible = false;
            lblAddTech.Visible = false;
        }

        private void lblAddTech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            cboTech.Visible = false;
            txtTechName.Visible = true;
            lblAddTech.Visible = false;
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            writer.openConnection();
            if (cboProjectName.Visible)
            {

            }

            else
            {
                string project_name = txtProjectName.Text;
                string desc = txtPDesc.Text;
                string item_name = txtPIName.Text;
                int project_code = Convert.ToInt32(cboProjectName.SelectedValue.ToString());
                string pi_desc = txtPIDesc.Text;
                string status = cboStatus.SelectedValue.ToString();
                int tech_code = Convert.ToInt32(cboTech.SelectedValue.ToString());

                lblMessage.Text = writer.insertProject(project_name, desc);
                lblMessage.Text += writer.insertProjectItem(item_name, project_code, pi_desc, status, tech_code);
                showProjects();
            }
            writer.closeConnection();
        }
    }
}
