﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Windows.Forms;

namespace PortfolioManager
{
    class SQLWriter
    {
        public OracleConnection conn;
        public void openConnection()
        {
            conn=new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            conn.Open();

        }
        public void closeConnection()
        {
            conn.Close();
        }

        public string insertMember(string uname, string fname, string lname, string pword, string bio, string photo)
        {
            
            OracleCommand cmd = new OracleCommand("pkg_add_portfolio_manager.add_user", conn);
            cmd.BindByName = true;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new OracleParameter("p_uname", OracleDbType.Varchar2, uname, ParameterDirection.Input));
            cmd.Parameters["p_uname"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_fname", OracleDbType.Varchar2, fname, ParameterDirection.Input));
            cmd.Parameters["p_fname"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_lname", OracleDbType.Varchar2, lname, ParameterDirection.Input));
            cmd.Parameters["p_lname"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_pword", OracleDbType.Varchar2, pword, ParameterDirection.Input));
            cmd.Parameters["p_pword"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_bio", OracleDbType.Varchar2, bio, ParameterDirection.Input));
            cmd.Parameters["p_bio"].Size = 200;
            cmd.Parameters.Add(new OracleParameter("p_photo", OracleDbType.Blob, photo, ParameterDirection.Input));
            cmd.Parameters["p_photo"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("error_message", OracleDbType.Varchar2, ParameterDirection.Output));
            cmd.Parameters["error_message"].Size = 50;

            cmd.ExecuteNonQuery();

            return cmd.Parameters["error_message"].Value.ToString();
        }

        public string insertProject(string project_name, string description)
        {

            OracleCommand cmd = new OracleCommand("pkg_add_portfolio_manager.add_project", conn);
            cmd.BindByName = true;
            cmd.CommandType = CommandType.StoredProcedure;

           
            cmd.Parameters.Add(new OracleParameter("p_project_name", OracleDbType.Varchar2, project_name, ParameterDirection.Input));
            cmd.Parameters["p_project_name"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_description", OracleDbType.Varchar2, description, ParameterDirection.Input));
            cmd.Parameters["p_description"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("error_message", OracleDbType.Varchar2, ParameterDirection.Output));
            cmd.Parameters["error_message"].Size = 50;

            cmd.ExecuteNonQuery();

            return cmd.Parameters["error_message"].Value.ToString();
        }

        public string insertProjectItem(string item_name, int project_code, string description, string status, int tech_code)
        {

            OracleCommand cmd = new OracleCommand("pkg_add_portfolio_manager.add_project_item", conn);
            cmd.BindByName = true;
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new OracleParameter("p_item_name", OracleDbType.Varchar2, item_name, ParameterDirection.Input));
            cmd.Parameters["p_item_name"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_project_code", OracleDbType.Int64, project_code, ParameterDirection.Input));
            cmd.Parameters["p_project_code"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_description", OracleDbType.Varchar2, description, ParameterDirection.Input));
            cmd.Parameters["p_description"].Size = 200;
            cmd.Parameters.Add(new OracleParameter("p_status", OracleDbType.Varchar2, status, ParameterDirection.Input));
            cmd.Parameters["p_status"].Size = 20;
            cmd.Parameters.Add(new OracleParameter("p_technology_code", OracleDbType.Int64, tech_code, ParameterDirection.Input));
            cmd.Parameters["p_technology_code"].Size = 200;
            cmd.Parameters.Add(new OracleParameter("error_message", OracleDbType.Varchar2, ParameterDirection.Output));
            cmd.Parameters["error_message"].Size = 50;


            cmd.ExecuteNonQuery();

            return cmd.Parameters["error_message"].Value.ToString();
        }

        public DataTable fillProjectDataSource(DataTable dt)
        {
            OracleDataAdapter da = new OracleDataAdapter();
                        
            da = new OracleDataAdapter("SELECT PROJECT_NAME, PROJECT_CODE FROM PROJECTS", conn);

            da.Fill(dt);

            return dt;
        }

        internal string getBio(string uname)
        {
            string queryString = "SELECT bio FROM Members where username = :username";
            OracleCommand getName = new OracleCommand(queryString, conn);
            getName.Parameters.Add(new OracleParameter("username", uname));
            return getName.ExecuteScalar().ToString();
        }

        internal string getFullName(string uname)
        {
            string fullName = getName(uname, "firstname");
            return fullName += " " + getName(uname, "lastname");

        }

        private string getName(string uname, string type)
        {
            string queryString = "SELECT "+type +" FROM Members where username = :username";
            OracleCommand getName = new OracleCommand(queryString, conn);
            getName.Parameters.Add(new OracleParameter("username", uname));
            return getName.ExecuteScalar().ToString();
        }

       

        public Boolean checkLoginDetails(string uname, string pword)
        {
            string queryString = "SELECT NVL(COUNT(*),0) FROM Members where username = :username AND p_word = :password";
            OracleCommand getUser = new OracleCommand(queryString, conn);
            getUser.Parameters.Add(new OracleParameter("username", uname));
            getUser.Parameters.Add(new OracleParameter("password", pword));

            int count = Convert.ToInt32(getUser.ExecuteScalar());

            return (count == 1);
        }

        internal bool userExists(string uname)
        {
            string queryString = "SELECT NVL(COUNT(*),0) FROM Members where username = :username";
            OracleCommand getUser = new OracleCommand(queryString, conn);
            getUser.Parameters.Add(new OracleParameter("username", uname));

            int count = Convert.ToInt32(getUser.ExecuteScalar());

            return (count == 1);
        }

        public void populateProjectRoles(DataGridView d, string type)
        {

            OracleCommand cmd = new OracleCommand("select * from "+type, conn);
            OracleDataReader reader = cmd.ExecuteReader();
            {
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);
                d.DataSource = dataTable;
            }
        }

        internal DataTable fillRoleDataSource(DataTable dt)
        {
            OracleDataAdapter da = new OracleDataAdapter();

            da = new OracleDataAdapter("SELECT ROLE_NAME, ROLE_CODE FROM PROJECTROLES", conn);

            da.Fill(dt);

            return dt;
        }

        internal DataTable fillTechDataSource(DataTable dt)
        {
            OracleDataAdapter da = new OracleDataAdapter();

            da = new OracleDataAdapter("SELECT TECH_NAME, CODE FROM TECHNOLOGIES", conn);

            da.Fill(dt);

            return dt;
        }

        internal DataTable fillStatusDataSource(DataTable dt)
        {
            OracleDataAdapter da = new OracleDataAdapter();

            da = new OracleDataAdapter("SELECT STATUS_NAME, CODE FROM STATUSES", conn);

            da.Fill(dt);

            return dt;
        }
    }
}
