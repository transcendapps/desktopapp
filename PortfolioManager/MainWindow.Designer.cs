﻿namespace PortfolioManager
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lnkNewUser = new System.Windows.Forms.LinkLabel();
            this.lblError = new System.Windows.Forms.Label();
            this.lblConfirmPassword = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.txtLastname = new System.Windows.Forms.TextBox();
            this.txtFirstname = new System.Windows.Forms.TextBox();
            this.tbcMainWindow = new System.Windows.Forms.TabControl();
            this.tbpLogin = new System.Windows.Forms.TabPage();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.tbpUserProfile = new System.Windows.Forms.TabPage();
            this.lblBio = new System.Windows.Forms.Label();
            this.lblProfileName = new System.Windows.Forms.Label();
            this.lblProfileUsername = new System.Windows.Forms.Label();
            this.picProfilePhoto = new System.Windows.Forms.PictureBox();
            this.tbpShowRoles = new System.Windows.Forms.TabPage();
            this.dgvRoleDetails = new System.Windows.Forms.DataGridView();
            this.tbpShowProjects = new System.Windows.Forms.TabPage();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtTechName = new System.Windows.Forms.TextBox();
            this.lblAddTech = new System.Windows.Forms.LinkLabel();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblNewProject = new System.Windows.Forms.LinkLabel();
            this.cboRole = new System.Windows.Forms.ComboBox();
            this.lblRole = new System.Windows.Forms.Label();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblTech = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.cboTech = new System.Windows.Forms.ComboBox();
            this.txtPIDesc = new System.Windows.Forms.TextBox();
            this.lblPIDesc = new System.Windows.Forms.Label();
            this.txtPIName = new System.Windows.Forms.TextBox();
            this.lblPIName = new System.Windows.Forms.Label();
            this.lblAddProject = new System.Windows.Forms.LinkLabel();
            this.cboProjectName = new System.Windows.Forms.ComboBox();
            this.txtPDesc = new System.Windows.Forms.TextBox();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.lblPDesc = new System.Windows.Forms.Label();
            this.lblProjectName = new System.Windows.Forms.Label();
            this.dgvProjects = new System.Windows.Forms.DataGridView();
            this.pROJECTSDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbcMainWindow.SuspendLayout();
            this.tbpLogin.SuspendLayout();
            this.tbpUserProfile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProfilePhoto)).BeginInit();
            this.tbpShowRoles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoleDetails)).BeginInit();
            this.tbpShowProjects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROJECTSDataTableBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(124, 67);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(100, 20);
            this.txtUsername.TabIndex = 3;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(124, 93);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(3, 70);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(58, 13);
            this.lblUsername.TabIndex = 2;
            this.lblUsername.Text = "User name";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(3, 96);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(149, 145);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lnkNewUser
            // 
            this.lnkNewUser.AutoSize = true;
            this.lnkNewUser.Location = new System.Drawing.Point(170, 171);
            this.lnkNewUser.Name = "lnkNewUser";
            this.lnkNewUser.Size = new System.Drawing.Size(54, 13);
            this.lnkNewUser.TabIndex = 5;
            this.lnkNewUser.TabStop = true;
            this.lnkNewUser.Text = "New User";
            this.lnkNewUser.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkNewUser_LinkClicked);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.SystemColors.Control;
            this.lblError.Location = new System.Drawing.Point(18, 157);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 12;
            // 
            // lblConfirmPassword
            // 
            this.lblConfirmPassword.AutoSize = true;
            this.lblConfirmPassword.Location = new System.Drawing.Point(3, 122);
            this.lblConfirmPassword.Name = "lblConfirmPassword";
            this.lblConfirmPassword.Size = new System.Drawing.Size(91, 13);
            this.lblConfirmPassword.TabIndex = 11;
            this.lblConfirmPassword.Text = "Confirm Password";
            this.lblConfirmPassword.Visible = false;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(3, 44);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(56, 13);
            this.lblLastName.TabIndex = 10;
            this.lblLastName.Text = "Last name";
            this.lblLastName.Visible = false;
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(6, 18);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(55, 13);
            this.lblFirstName.TabIndex = 9;
            this.lblFirstName.Text = "First name";
            this.lblFirstName.Visible = false;
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Location = new System.Drawing.Point(124, 119);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(100, 20);
            this.txtConfirmPassword.TabIndex = 5;
            this.txtConfirmPassword.Visible = false;
            // 
            // txtLastname
            // 
            this.txtLastname.Location = new System.Drawing.Point(124, 41);
            this.txtLastname.Name = "txtLastname";
            this.txtLastname.Size = new System.Drawing.Size(100, 20);
            this.txtLastname.TabIndex = 2;
            this.txtLastname.Visible = false;
            // 
            // txtFirstname
            // 
            this.txtFirstname.Location = new System.Drawing.Point(124, 15);
            this.txtFirstname.Name = "txtFirstname";
            this.txtFirstname.Size = new System.Drawing.Size(100, 20);
            this.txtFirstname.TabIndex = 1;
            this.txtFirstname.Visible = false;
            // 
            // tbcMainWindow
            // 
            this.tbcMainWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcMainWindow.Controls.Add(this.tbpLogin);
            this.tbcMainWindow.Controls.Add(this.tbpUserProfile);
            this.tbcMainWindow.Controls.Add(this.tbpShowRoles);
            this.tbcMainWindow.Controls.Add(this.tbpShowProjects);
            this.tbcMainWindow.Location = new System.Drawing.Point(12, 12);
            this.tbcMainWindow.Name = "tbcMainWindow";
            this.tbcMainWindow.SelectedIndex = 0;
            this.tbcMainWindow.Size = new System.Drawing.Size(578, 314);
            this.tbcMainWindow.TabIndex = 13;
            // 
            // tbpLogin
            // 
            this.tbpLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.tbpLogin.Controls.Add(this.btnAddUser);
            this.tbpLogin.Controls.Add(this.lblError);
            this.tbpLogin.Controls.Add(this.lblFirstName);
            this.tbpLogin.Controls.Add(this.lnkNewUser);
            this.tbpLogin.Controls.Add(this.txtConfirmPassword);
            this.tbpLogin.Controls.Add(this.btnLogin);
            this.tbpLogin.Controls.Add(this.lblConfirmPassword);
            this.tbpLogin.Controls.Add(this.txtFirstname);
            this.tbpLogin.Controls.Add(this.lblLastName);
            this.tbpLogin.Controls.Add(this.lblUsername);
            this.tbpLogin.Controls.Add(this.txtUsername);
            this.tbpLogin.Controls.Add(this.txtPassword);
            this.tbpLogin.Controls.Add(this.lblPassword);
            this.tbpLogin.Controls.Add(this.txtLastname);
            this.tbpLogin.Location = new System.Drawing.Point(4, 22);
            this.tbpLogin.Name = "tbpLogin";
            this.tbpLogin.Padding = new System.Windows.Forms.Padding(3);
            this.tbpLogin.Size = new System.Drawing.Size(570, 288);
            this.tbpLogin.TabIndex = 0;
            this.tbpLogin.Text = "Login";
            // 
            // btnAddUser
            // 
            this.btnAddUser.Location = new System.Drawing.Point(149, 145);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(75, 23);
            this.btnAddUser.TabIndex = 6;
            this.btnAddUser.Text = "Add User";
            this.btnAddUser.UseVisualStyleBackColor = true;
            this.btnAddUser.Visible = false;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // tbpUserProfile
            // 
            this.tbpUserProfile.BackColor = System.Drawing.Color.Transparent;
            this.tbpUserProfile.Controls.Add(this.lblBio);
            this.tbpUserProfile.Controls.Add(this.lblProfileName);
            this.tbpUserProfile.Controls.Add(this.lblProfileUsername);
            this.tbpUserProfile.Controls.Add(this.picProfilePhoto);
            this.tbpUserProfile.Location = new System.Drawing.Point(4, 22);
            this.tbpUserProfile.Name = "tbpUserProfile";
            this.tbpUserProfile.Padding = new System.Windows.Forms.Padding(3);
            this.tbpUserProfile.Size = new System.Drawing.Size(570, 288);
            this.tbpUserProfile.TabIndex = 1;
            this.tbpUserProfile.Text = "Profile";
            // 
            // lblBio
            // 
            this.lblBio.AutoSize = true;
            this.lblBio.Location = new System.Drawing.Point(199, 105);
            this.lblBio.Name = "lblBio";
            this.lblBio.Size = new System.Drawing.Size(35, 13);
            this.lblBio.TabIndex = 4;
            this.lblBio.Text = "label1";
            // 
            // lblProfileName
            // 
            this.lblProfileName.AutoSize = true;
            this.lblProfileName.Location = new System.Drawing.Point(199, 77);
            this.lblProfileName.Name = "lblProfileName";
            this.lblProfileName.Size = new System.Drawing.Size(35, 13);
            this.lblProfileName.TabIndex = 3;
            this.lblProfileName.Text = "label2";
            // 
            // lblProfileUsername
            // 
            this.lblProfileUsername.AutoSize = true;
            this.lblProfileUsername.Location = new System.Drawing.Point(199, 46);
            this.lblProfileUsername.Name = "lblProfileUsername";
            this.lblProfileUsername.Size = new System.Drawing.Size(35, 13);
            this.lblProfileUsername.TabIndex = 2;
            this.lblProfileUsername.Text = "label1";
            // 
            // picProfilePhoto
            // 
            this.picProfilePhoto.Location = new System.Drawing.Point(6, 6);
            this.picProfilePhoto.Name = "picProfilePhoto";
            this.picProfilePhoto.Size = new System.Drawing.Size(114, 146);
            this.picProfilePhoto.TabIndex = 1;
            this.picProfilePhoto.TabStop = false;
            // 
            // tbpShowRoles
            // 
            this.tbpShowRoles.Controls.Add(this.dgvRoleDetails);
            this.tbpShowRoles.Location = new System.Drawing.Point(4, 22);
            this.tbpShowRoles.Name = "tbpShowRoles";
            this.tbpShowRoles.Size = new System.Drawing.Size(570, 288);
            this.tbpShowRoles.TabIndex = 2;
            this.tbpShowRoles.Text = "Project Roles";
            this.tbpShowRoles.UseVisualStyleBackColor = true;
            // 
            // dgvRoleDetails
            // 
            this.dgvRoleDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRoleDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRoleDetails.Location = new System.Drawing.Point(0, 3);
            this.dgvRoleDetails.Name = "dgvRoleDetails";
            this.dgvRoleDetails.Size = new System.Drawing.Size(567, 212);
            this.dgvRoleDetails.TabIndex = 0;
            // 
            // tbpShowProjects
            // 
            this.tbpShowProjects.Controls.Add(this.lblMessage);
            this.tbpShowProjects.Controls.Add(this.txtTechName);
            this.tbpShowProjects.Controls.Add(this.lblAddTech);
            this.tbpShowProjects.Controls.Add(this.btnBack);
            this.tbpShowProjects.Controls.Add(this.lblNewProject);
            this.tbpShowProjects.Controls.Add(this.cboRole);
            this.tbpShowProjects.Controls.Add(this.lblRole);
            this.tbpShowProjects.Controls.Add(this.btnAddItem);
            this.tbpShowProjects.Controls.Add(this.lblStatus);
            this.tbpShowProjects.Controls.Add(this.lblTech);
            this.tbpShowProjects.Controls.Add(this.cboStatus);
            this.tbpShowProjects.Controls.Add(this.cboTech);
            this.tbpShowProjects.Controls.Add(this.txtPIDesc);
            this.tbpShowProjects.Controls.Add(this.lblPIDesc);
            this.tbpShowProjects.Controls.Add(this.txtPIName);
            this.tbpShowProjects.Controls.Add(this.lblPIName);
            this.tbpShowProjects.Controls.Add(this.lblAddProject);
            this.tbpShowProjects.Controls.Add(this.cboProjectName);
            this.tbpShowProjects.Controls.Add(this.txtPDesc);
            this.tbpShowProjects.Controls.Add(this.txtProjectName);
            this.tbpShowProjects.Controls.Add(this.lblPDesc);
            this.tbpShowProjects.Controls.Add(this.lblProjectName);
            this.tbpShowProjects.Controls.Add(this.dgvProjects);
            this.tbpShowProjects.Location = new System.Drawing.Point(4, 22);
            this.tbpShowProjects.Name = "tbpShowProjects";
            this.tbpShowProjects.Size = new System.Drawing.Size(570, 288);
            this.tbpShowProjects.TabIndex = 3;
            this.tbpShowProjects.Text = "Project Details";
            this.tbpShowProjects.UseVisualStyleBackColor = true;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(328, 180);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 13);
            this.lblMessage.TabIndex = 29;
            // 
            // txtTechName
            // 
            this.txtTechName.Location = new System.Drawing.Point(165, 137);
            this.txtTechName.Name = "txtTechName";
            this.txtTechName.Size = new System.Drawing.Size(100, 20);
            this.txtTechName.TabIndex = 28;
            this.txtTechName.Visible = false;
            // 
            // lblAddTech
            // 
            this.lblAddTech.AutoSize = true;
            this.lblAddTech.Location = new System.Drawing.Point(278, 145);
            this.lblAddTech.Name = "lblAddTech";
            this.lblAddTech.Size = new System.Drawing.Size(85, 13);
            this.lblAddTech.TabIndex = 27;
            this.lblAddTech.TabStop = true;
            this.lblAddTech.Text = "Add Technology";
            this.lblAddTech.Visible = false;
            this.lblAddTech.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddTech_LinkClicked);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(288, 244);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 26;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblNewProject
            // 
            this.lblNewProject.AutoSize = true;
            this.lblNewProject.Location = new System.Drawing.Point(52, 244);
            this.lblNewProject.Name = "lblNewProject";
            this.lblNewProject.Size = new System.Drawing.Size(88, 13);
            this.lblNewProject.TabIndex = 25;
            this.lblNewProject.TabStop = true;
            this.lblNewProject.Text = "New Project Item";
            this.lblNewProject.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNewProject_LinkClicked);
            // 
            // cboRole
            // 
            this.cboRole.FormattingEnabled = true;
            this.cboRole.Location = new System.Drawing.Point(144, 163);
            this.cboRole.Name = "cboRole";
            this.cboRole.Size = new System.Drawing.Size(121, 21);
            this.cboRole.TabIndex = 24;
            this.cboRole.Visible = false;
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Location = new System.Drawing.Point(76, 171);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(32, 13);
            this.lblRole.TabIndex = 23;
            this.lblRole.Text = "Role:";
            this.lblRole.Visible = false;
            // 
            // btnAddItem
            // 
            this.btnAddItem.Location = new System.Drawing.Point(190, 222);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(75, 23);
            this.btnAddItem.TabIndex = 22;
            this.btnAddItem.Text = "Add Item";
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Visible = false;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(32, 118);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(76, 13);
            this.lblStatus.TabIndex = 21;
            this.lblStatus.Text = "Project Status:";
            this.lblStatus.Visible = false;
            // 
            // lblTech
            // 
            this.lblTech.AutoSize = true;
            this.lblTech.Location = new System.Drawing.Point(42, 144);
            this.lblTech.Name = "lblTech";
            this.lblTech.Size = new System.Drawing.Size(66, 13);
            this.lblTech.TabIndex = 20;
            this.lblTech.Text = "Technology:";
            this.lblTech.Visible = false;
            // 
            // cboStatus
            // 
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Location = new System.Drawing.Point(144, 110);
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.Size = new System.Drawing.Size(121, 21);
            this.cboStatus.TabIndex = 18;
            this.cboStatus.Visible = false;
            // 
            // cboTech
            // 
            this.cboTech.FormattingEnabled = true;
            this.cboTech.Location = new System.Drawing.Point(144, 137);
            this.cboTech.Name = "cboTech";
            this.cboTech.Size = new System.Drawing.Size(121, 21);
            this.cboTech.TabIndex = 17;
            this.cboTech.Visible = false;
            // 
            // txtPIDesc
            // 
            this.txtPIDesc.Location = new System.Drawing.Point(165, 84);
            this.txtPIDesc.Name = "txtPIDesc";
            this.txtPIDesc.Size = new System.Drawing.Size(100, 20);
            this.txtPIDesc.TabIndex = 15;
            this.txtPIDesc.Visible = false;
            // 
            // lblPIDesc
            // 
            this.lblPIDesc.AutoSize = true;
            this.lblPIDesc.Location = new System.Drawing.Point(22, 91);
            this.lblPIDesc.Name = "lblPIDesc";
            this.lblPIDesc.Size = new System.Drawing.Size(86, 13);
            this.lblPIDesc.TabIndex = 14;
            this.lblPIDesc.Text = "Item Description:";
            this.lblPIDesc.Visible = false;
            // 
            // txtPIName
            // 
            this.txtPIName.Location = new System.Drawing.Point(165, 58);
            this.txtPIName.Name = "txtPIName";
            this.txtPIName.Size = new System.Drawing.Size(100, 20);
            this.txtPIName.TabIndex = 13;
            this.txtPIName.Visible = false;
            // 
            // lblPIName
            // 
            this.lblPIName.AutoSize = true;
            this.lblPIName.Location = new System.Drawing.Point(11, 65);
            this.lblPIName.Name = "lblPIName";
            this.lblPIName.Size = new System.Drawing.Size(97, 13);
            this.lblPIName.TabIndex = 12;
            this.lblPIName.Text = "Project Item Name:";
            this.lblPIName.Visible = false;
            // 
            // lblAddProject
            // 
            this.lblAddProject.AutoSize = true;
            this.lblAddProject.Location = new System.Drawing.Point(278, 38);
            this.lblAddProject.Name = "lblAddProject";
            this.lblAddProject.Size = new System.Drawing.Size(62, 13);
            this.lblAddProject.TabIndex = 10;
            this.lblAddProject.TabStop = true;
            this.lblAddProject.Text = "Add Project";
            this.lblAddProject.Visible = false;
            this.lblAddProject.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddProject_LinkClicked);
            // 
            // cboProjectName
            // 
            this.cboProjectName.DisplayMember = "none";
            this.cboProjectName.FormattingEnabled = true;
            this.cboProjectName.Location = new System.Drawing.Point(144, 31);
            this.cboProjectName.Name = "cboProjectName";
            this.cboProjectName.Size = new System.Drawing.Size(121, 21);
            this.cboProjectName.TabIndex = 9;
            this.cboProjectName.ValueMember = "none";
            this.cboProjectName.Visible = false;
            // 
            // txtPDesc
            // 
            this.txtPDesc.Location = new System.Drawing.Point(351, 31);
            this.txtPDesc.Name = "txtPDesc";
            this.txtPDesc.Size = new System.Drawing.Size(100, 20);
            this.txtPDesc.TabIndex = 8;
            this.txtPDesc.Visible = false;
            // 
            // txtProjectName
            // 
            this.txtProjectName.Location = new System.Drawing.Point(165, 31);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(100, 20);
            this.txtProjectName.TabIndex = 7;
            this.txtProjectName.Visible = false;
            // 
            // lblPDesc
            // 
            this.lblPDesc.AutoSize = true;
            this.lblPDesc.Location = new System.Drawing.Point(285, 34);
            this.lblPDesc.Name = "lblPDesc";
            this.lblPDesc.Size = new System.Drawing.Size(60, 13);
            this.lblPDesc.TabIndex = 6;
            this.lblPDesc.Text = "Description";
            this.lblPDesc.Visible = false;
            // 
            // lblProjectName
            // 
            this.lblProjectName.AutoSize = true;
            this.lblProjectName.Location = new System.Drawing.Point(34, 34);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(74, 13);
            this.lblProjectName.TabIndex = 5;
            this.lblProjectName.Text = "Project Name:";
            this.lblProjectName.Visible = false;
            // 
            // dgvProjects
            // 
            this.dgvProjects.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProjects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProjects.Location = new System.Drawing.Point(3, 3);
            this.dgvProjects.Name = "dgvProjects";
            this.dgvProjects.Size = new System.Drawing.Size(564, 213);
            this.dgvProjects.TabIndex = 0;
            // 
            // pROJECTSDataTableBindingSource
            // 
            this.pROJECTSDataTableBindingSource.DataSource = typeof(PortfolioManager.PortfolioDataSet.PROJECTSDataTable);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(602, 338);
            this.Controls.Add(this.tbcMainWindow);
            this.Name = "MainWindow";
            this.Text = "PortfolioManager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tbcMainWindow.ResumeLayout(false);
            this.tbpLogin.ResumeLayout(false);
            this.tbpLogin.PerformLayout();
            this.tbpUserProfile.ResumeLayout(false);
            this.tbpUserProfile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProfilePhoto)).EndInit();
            this.tbpShowRoles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoleDetails)).EndInit();
            this.tbpShowProjects.ResumeLayout(false);
            this.tbpShowProjects.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROJECTSDataTableBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.LinkLabel lnkNewUser;
        private System.Windows.Forms.TextBox txtLastname;
        private System.Windows.Forms.TextBox txtFirstname;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label lblConfirmPassword;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.TabControl tbcMainWindow;
        private System.Windows.Forms.TabPage tbpLogin;
        private System.Windows.Forms.TabPage tbpUserProfile;
        private System.Windows.Forms.Button btnAddUser;
        private System.Windows.Forms.TabPage tbpShowRoles;
        private System.Windows.Forms.TabPage tbpShowProjects;
        private System.Windows.Forms.DataGridView dgvRoleDetails;
        private System.Windows.Forms.Label lblBio;
        private System.Windows.Forms.Label lblProfileName;
        private System.Windows.Forms.Label lblProfileUsername;
        private System.Windows.Forms.PictureBox picProfilePhoto;
        private System.Windows.Forms.DataGridView dgvProjects;
        private System.Windows.Forms.TextBox txtPDesc;
        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.Label lblPDesc;
        private System.Windows.Forms.Label lblProjectName;
        private System.Windows.Forms.LinkLabel lblAddProject;
        private System.Windows.Forms.ComboBox cboProjectName;
        private System.Windows.Forms.Label lblPIName;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblTech;
        private System.Windows.Forms.ComboBox cboStatus;
        private System.Windows.Forms.ComboBox cboTech;
        private System.Windows.Forms.TextBox txtPIDesc;
        private System.Windows.Forms.Label lblPIDesc;
        private System.Windows.Forms.TextBox txtPIName;
        private System.Windows.Forms.ComboBox cboRole;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.LinkLabel lblNewProject;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.BindingSource pROJECTSDataTableBindingSource;
        private System.Windows.Forms.TextBox txtTechName;
        private System.Windows.Forms.LinkLabel lblAddTech;
        private System.Windows.Forms.Label lblMessage;
    }
}

